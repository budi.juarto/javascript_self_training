// Modify the function sum using the rest parameter in such a way that 
// the function sum is able to take any number of arguments and return their sum.

//buat argument dimasukan bukan inputan x,y,z agar lebih fleksibel dalam input
const sum = (...args) => {
    // const args = [x, y, z];
    return args.reduce((a, b) => a + b, 0);
  }
  console.log(sum(1, 2, 3)); // 6
  

