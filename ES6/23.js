// There are two functions related to strings in the editor.
// Export both of them using the method of your choice.

export const uppercaseString = (string) => {
    return string.toUpperCase();
  }
  
  export const lowercaseString = (string) => {
    return string.toLowerCase()
  }
  
  