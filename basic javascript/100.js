/*Write a recursive function, sum(arr, n), that returns the sum of the elements
from 0 to n inclusive in an array arr. */

function sum(arr, n) {
    // Only change code below this line
    if (n <=0){
      return arr[0]
    } else{
      //recursion memudahkan kita dalam loop
      // jadi pada contoh n =1 maka hanya di loop 1 kali
      // sum([2, 3, 4], 1) maka nilainya 5 dari 2+3
      return sum(arr, n-1) + arr[n];
    }
    // Only change code above this line
  }
  