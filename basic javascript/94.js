// Use a for loop to work to push the values 1 through 5 onto myArray.

// Example
var ourArray = [];

for (var i = 0; i < 5; i++) {
  ourArray.push(i);
}

// Setup
var myArray = [];

for (var i = 1; i <= 5; i++) {
  myArray.push(i);
}
// Only change code below this line.
console.log(myArray)