// Set someAdjective and append it to myStr using the += operator.

// Example
var anAdjective = "awesome!";
var ourStr = "freeCodeCamp is ";
ourStr += anAdjective;

// Only change code below this line

var someAdjective = "  GG";
var myStr = "Learning to code is ";
myStr += someAdjective;