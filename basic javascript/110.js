/*
We have defined a function called countdown with two parameters. 
The function should take an array in the myArray parameter and append the numbers
 n through 1 based on the n parameter.
For example, calling this function with n = 5 will pad the array with the numbers 
[5, 4, 3, 2, 1] inside of it. Your function must use recursion by calling itself and must 
not use loops of any kind
*/

//Only change code below this line
function countdown(myArray, n){
    //buat logika if jika 0
    if (n <=0){
      return;
    }
    myArray.push(n); //di atas biar 5 ikut masuk
    countdown(myArray, n-1) // dikurang hinga 0 dan jika    0 return ga ada
    
  
  }
  