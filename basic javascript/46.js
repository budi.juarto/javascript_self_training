
// Create a function called reusableFunction which prints "Hi World" to the dev console.
// Call the function.

// Example
function ourReusableFunction() {
    console.log("Heyya, World");
  }
  
  function reusableFunction() {
      console.log("Hi World");
  }
  
  reusableFunction()
  
  // Only change code below this line
  