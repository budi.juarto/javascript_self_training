// Push the odd numbers from 1 through 9 to myArray using a for loop.

// Example
var ourArray = [];

for (var i = 0; i < 10; i += 2) {
  if( i%2!=0){
   ourArray.push(i);

  }
}

// Setup
var myArray = [];
for (var i = 0; i < 10; i++) {
  if( i%2!=0){
    myArray.push(i);

  }
}
// Only change code below this line.
