/*Modify the function checkObj to test myObj for checkProp.
If the property is found, return that property's value. If not, return "Not Found".*/

// Setup
var myObj = {
    gift: "pony",
    pet: "kitten",
    bed: "sleigh"
  };
  
  function checkObj(checkProp) {
    // percabangan ada atau tidak barangnya
    if(myObj.hasOwnProperty(checkProp)== true){
      return myObj[checkProp];
    }
    else if (myObj.hasOwnProperty(checkProp)== false ){
      return "Not Found";
    }
  }
  
  // Test your code by modifying these values
  checkObj("gift");
  